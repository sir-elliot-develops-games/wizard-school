function formatTime(hours,minutes) {
  text = "";
  if (hours > 12) {
    text += (hours - 12);
  }
  else if (hours == 0) {
    text += "12";
  }
  else {
    text += hours;
  }
  text += ":";
  if (minutes < 10) {
    text += "0" + minutes;
  }
  else {
    text += minutes;
  }
  text += " ";
  if (hours >= 12) {
    text += "pm";
  }
  else {
    text += "am";
  }
  return text;
}

function timePasses() {
  gameData.minutes += 1;
  if (gameData.minutes >= 60) {
    gameData.hours += 1;
    gameData.minutes -= 60;
  }
  if (gameData.hours >= 24) {
    gameData.days += 1;
    gameData.hours -= 24;
  }
  document.getElementById("gametime").innerHTML = "Day " + gameData.days + ", " + formatTime(gameData.hours,gameData.minutes);
}
