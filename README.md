URL: https://sir-elliot-develops-games.gitlab.io/wizard-school/

### Game Play Loop

Generate mana. This is the main currency. 
Mana buys spell crystals. 

Spend mana to research spells. 
Spells generate mana in different ways. Spells are choices: picking one blocks off others of the same tier.
Spells unlock new quests/story bits.



Goal is to generate enough power to prevent/cause an apocalypse (there's a choice at the end -- the apocalypse that's brewing could've been caused by the player from the future.)
There are two prestige loops. One is a time spell that lets you restart the spell learning process in order to generate faster. If you're not powerful enough when the apocalypse looms then you can restart time.


There's also another prestige loop-- after the world ends/survives, the player can travel to another dimension? Need to think more on this.     


