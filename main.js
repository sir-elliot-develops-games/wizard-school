var gameData = {
    minutes: 0,
    hours: 0,
    days: 1,
    numMana: 0,
    costCrystal: 20,
    numCrystals: 0,
    crystalMult: 1,
    spellsDone: [],
    marketStock: [],
    clickMana: 1,
    questList: [],
    costRed: 40,
    numRed: 0,
};


document.getElementById("crystalBuy").style.display = "none";
document.getElementById("redBuy").style.display = "none";


function tab(tab) {
  // hide all your tabs, then show the one the user selected.
    document.getElementById("pageOne").style.display = "none";
    document.getElementById("pageLog").style.display = "none";
    document.getElementById("pageTwo").style.display = "none";
    document.getElementById("pageMap").style.display = "none";
    document.getElementById("pagePond").style.display = "none";
    document.getElementById("pageMarket").style.display = "none";
    document.getElementById(tab).style.display = "inline-block";
    if (tab === "pageMarket") {
        updateMarket();
    }
}



//Initialize the page
tab("pageOne");



//currencies
document.getElementById("crystals").style.display = "none";
document.getElementById("redcrystals").style.display = "none";


function showUpdate() {
    document.getElementById("mana").innerHTML = "Mana: " + gameData.numMana;
    document.getElementById("crystals").innerHTML = "Blue Crystals: " + gameData.numCrystals;
    document.getElementById("redcrystals").innerHTML = "Red Crystals: " + gameData.numRed;
    document.getElementById("crystalBuy").innerHTML = "Buy Blue Crystal (" + gameData.costCrystal + " Mana)";
    document.getElementById("redBuy").innerHTML = "Buy Red Crystal (" + gameData.costRed + " Mana)";
    if (gameData.numMana < gameData.costCrystal) {
        document.getElementById("crystalBuy").disabled = true;
    } else {
        document.getElementById("crystalBuy").disabled = false;
        document.getElementById("crystalBuy").style.display = "inline-block";
    };
    if (gameData.numMana < gameData.costRed) {
        document.getElementById("redBuy").disabled = true;
    } else {
        document.getElementById("redBuy").disabled = false;
        document.getElementById("redBuy").style.display = "inline-block";
    };
    storyUpdate();
}

function updateCurrency() {
    var manaTick = gameData.numCrystals * gameData.crystalMult;
    gameData.numMana += manaTick;
    document.getElementById("manatick").innerHTML = manaTick;
}

function manaUp() {
    //increment whatever variable is relevant
    gameData.numMana += gameData.clickMana + gameData.numRed;
    showUpdate();
}

function buyCrystals() {
    gameData.numMana -= gameData.costCrystal;
    gameData.numCrystals += 1;
    gameData.costCrystal += gameData.numCrystals * 2;
    document.getElementById("crystals").style.display = "inline-block";
    updateCurrency();
    showUpdate();
}

function buyRed() {
    gameData.numMana -= gameData.costRed;
    gameData.numRed += 1;
    gameData.costRed += gameData.numRed * 4;
    document.getElementById("redcrystals").style.display = "inline-block";
    document.getElementById("buttonmana").innerHTML = gameData.clickMana + gameData.numRed;
    showUpdate();
}

var mainGameLoop = window.setInterval(function () {
    storyUpdate();
    timePasses();
    updateCurrency();
    showUpdate();
/*  changeBackground(); */ /* maybe some day the background will change with the time */
}, 500);

