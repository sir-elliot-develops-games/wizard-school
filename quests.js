function addQuest(name,description,status) {
  gameData.questList.push(new Quest(name,description,status));
  updateQuests();
}


function updateQuests() {
  var quest_text = "";
  var i;
  for (i = 0; i < gameData.questList.length; i++) {
    if (gameData.questList[i].status != "Unknown") {
      quest_text += "<div class='tooltip'>" + gameData.questList[i].name + "<span class='qtooltiptext'>" + gameData.questList[i].description + "</span></div>" + "<br>"
    }
  }
  document.getElementById("quests").innerHTML = quest_text;
}

class Quest {
  constructor(name, description, status) {
    this.name = name; //name of quest
    this.description = description; //description for player
    this.status = status; // status: Unknown, Open, or Closed
  }
}

// Put all quests here.

addQuest("Test Quest!","This quest is a test. For me, not for you.","Open");
