
var log_text = "";

function storyUpdate() {
    if (gameData.numCrystals == 1) {
        storyPrint("crystal1");
    }
    if (gameData.numCrystals == 2) {
        storyPrint("crystal2");
    }
    if (gameData.numRed == 1) {
        storyPrint("red1");
    }
    if (gameData.numMana == 0) {
        manaPrint(0);
    }
    if (gameData.numMana == 10) {
        manaPrint(10);
    }
    if (gameData.numMana == 20) {
        manaPrint(20);
    }
    if (gameData.numMana >= 100) {
        manaPrint(100);
    }
}


function manaPrint(x) {
    if (x in manaStory) {
        document.getElementById("story_text").innerHTML = manaStory[x];
        var new_text = manaStory[x] + "<br>";
        log_text = new_text.concat(log_text);
        document.getElementById("log_text").innerHTML = log_text;
        delete manaStory[x];
        document.getElementById(nav2).style.display = "inline-block";
    }
}

function storyPrint(x) {
    if (x in miscStory){
        document.getElementById("story_text").innerHTML = miscStory[x];
        var new_text = miscStory[x] + "<br>";
        log_text = new_text.concat(log_text);
        document.getElementById("log_text").innerHTML = log_text;
        delete miscStory[x];
        document.getElementById(nav2).style.display = "inline-block";
    }
}

var manaStory = {
    0: "They treat us students like dirt here, manual labor for them to exploit. But the skills I've learned... could I generate my own mana?",
    10: "A decent start, but I'll need more if I want to do anything interesting.",
    20: "I bet I could compress this mana into a crystal... but what would I use that for?",
    100: "Whoa! Now we're talking. Hey, this is almost enough to start casting spells..."
}


var miscStory = {
    "crystal1": "The crystal pulses with power. It's generating mana! Thank god, I can take a break!",
    "crystal2": "More crystals, more power. If I acquired enough mana, I could do anything...",
    "red1": "Oooh, a new color! Now what the hell does this do?",
}